SITE_URL = 'http://{{ inventory_hostname }}'
MEDIA_ROOT = '{{ storage_dir }}'
DEFAULT_FROM_EMAIL = '{{ project_name }} <noreply@{{ inventory_hostname }}>'
TRYDIFFOSCOPE_MAX_UPLOAD_SIZE_MEGABYTES = '{{ client_max_body_size_megabytes }}'
