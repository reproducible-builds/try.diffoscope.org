import os
import shutil
import tempfile
import subprocess

from django.conf import settings

from . import app_settings


def call_in_container(args, cwd):
    tempdir = tempfile.mkdtemp(prefix='trydiffoscope-')

    # seccomp=unconfined is added due to
    # <https://medium.com/nttlabs/ubuntu-21-10-and-fedora-35-do-not-work-on-docker-20-10-9-1cd439d9921>
    all_args = (
        [
            'docker',
            'run',
            '--rm=true',
            '--net=none',
            '--user',
            app_settings.DOCKER_USER,
            '--read-only=true',
            '--memory',
            app_settings.DOCKER_MEMORY_LIMIT,
            '--cidfile',
            os.path.join(tempdir, 'cidfile'),
            '--workdir',
            cwd,
            '--volume',
            '%s:%s' % (cwd, cwd),
            '--security-opt',
            'seccomp=unconfined',
        ]
        + ['--cap-drop=%s' % x for x in app_settings.DOCKER_DROP_CAPABILITIES]
        + [app_settings.DOCKER_IMAGE]
        + list(args)
    )

    # If we aren't using a container, just call diffoscope directly.
    if not settings.TRYDIFFOSCOPE_USE_CONTAINER:
        all_args = ['diffoscope'] + list(args)

    p = subprocess.Popen(
        all_args, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    return p, tempdir


def kill_container(tempdir):
    try:
        with open(os.path.join(tempdir, 'cidfile')) as f:
            container_id = f.read()
    except IOError:
        pass
    else:
        subprocess.call(
            ('docker', 'rm', '--force', container_id),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )

    shutil.rmtree(tempdir, ignore_errors=True)


def clean_images():
    subprocess.call(
        ('docker ps --all --quiet | xargs -r -L 1 docker rm',), shell=True
    )


def clean_containers():
    subprocess.call(
        (
            'docker images --filter dangling=true --quiet | '
            'xargs -r -L 1 docker rmi',
        ),
        shell=True,
    )


def pull_image():
    subprocess.call(('docker', 'pull', app_settings.DOCKER_IMAGE))
