from django.core.management.base import BaseCommand

from ...utils import pull_image


class Command(BaseCommand):
    def handle(self, *args, **options):
        pull_image()
