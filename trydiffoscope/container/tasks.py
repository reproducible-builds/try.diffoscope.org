import celery

from . import utils


@celery.task()
def cleanup():
    utils.clean_images()
    utils.clean_containers()


@celery.task()
def pull_image():
    utils.pull_image()
