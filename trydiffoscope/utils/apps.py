from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = 'trydiffoscope.utils'

    def ready(self):
        # Celery app must be imported on startup
        from ..celery_app import app  # noqa
